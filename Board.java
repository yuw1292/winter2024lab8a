public class Board {
	private Tile[][] grid;

	public Board() {
		this.grid = new Tile[3][3];
		for (int row = 0; row < grid.length; row++) {
			for (int col = 0; col < grid.length; col++) {
				grid[row][col] = Tile.BLANK;
			}
		}
	}
	public String toString() {
		String board = "";
		for (int row = 0; row < 3; row++) {
			for (int col = 0; col < 3; col++) {
				board += grid[row][col].getName() + " ";
			}
			board += "\n";
		}
		return board;
	}

    public boolean placeToken(int row, int col, Tile playerToken) {
		if (row < 0 || row >= 3 || col < 0 || col >= 3) {
			return false;
		}
		if (grid[row][col] != Tile.BLANK) {
			return false;
		}
		grid[row][col] = playerToken;
		return true;
	}

	private boolean checkIfWinningHorizontal(Tile playerToken) {
        for (int row = 0; row < grid.length; row++) {
			if (this.grid[row][0] == playerToken && this.grid[row][1] == playerToken && this.grid[row][2] == playerToken) {
				return true;
			}
		}
		return false;
	}

	private boolean checkIfWinningVertical(Tile playerToken) {
        for (int col = 0; col < grid.length; col++) {
            if (this.grid[0][col] == playerToken && this.grid[1][col] == playerToken && this.grid[2][col] == playerToken) {
                return true;
            }
        }
        return false;
    }

    public boolean checkIfWinning(Tile playerToken) {
        return checkIfWinningHorizontal(playerToken) || checkIfWinningVertical(playerToken);
    }
	
	public boolean checkIfFull() {
		for (int row = 0; row < 3; row++) {
			for (int col = 0; col < 3; col++) {
				if (grid[row][col] == Tile.BLANK) {
					return false;
				}
			}
		}
		return true;
	}
}
	