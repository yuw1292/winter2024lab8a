import java.util.Scanner;

public class TicTacToeApp {

	public static void main(String[] args) {
		System.out.println("Welcome to the Tic Tac Toe game!");
		System.out.println("Player 1's token: X");
		System.out.println("Player 2's token: O");

		Board board = new Board();
        boolean gameOver = false;
        int player = 1;
        Tile playerToken = Tile.X;

        Scanner scanner = new Scanner(System.in);

        while (!gameOver) {
            System.out.println(board);

            System.out.println("Player " + player + "'s turn. Where do you want to plave your token? (row col)");

            int row = scanner.nextInt() - 1;
            int col = scanner.nextInt() - 1;
			
			if (player == 1) {
				playerToken = Tile.X;
			} else {
				playerToken = Tile.O;
			}

			while (!board.placeToken(row, col, playerToken)) {
				System.out.println("Invalid move! Please try again");
                System.out.println("Where do you want to plave your token? (row col)");
				row = scanner.nextInt();
				col = scanner.nextInt();
			}

            if (board.checkIfWinning(playerToken)) {
                System.out.println("Player " + player + " wins!");
                gameOver = true;
            } else if (board.checkIfFull()) {
                System.out.println("It's a tie!");
                gameOver = true;
            } else {
                player++;
                if (player > 2) {
                    player = 1;
                }
            }
        }
    }
}
